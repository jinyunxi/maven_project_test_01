﻿package cn.zr.action;

/**
 * 2019.1.8
 * @author zhan_rong
 *2019.1.8
 */

public class CustomerAction {

	private Long custId;
	public Long getCustId() {
		return custId;
	}
	public void setCustId(Long custId) {
		this.custId = custId;
	}
	public String querycustomer() {
		System.out.println("客户请求客户Id：" + custId);
		return "success";
	}
}
